
/**
 * Initialize the uc_location module code: add the "apply_address()"
 **/
Drupal.behaviors.ucLocationInitialize = function(elt) {
  // 'elt' will be the document the first time this gets called; may be anything at any other time
  
  if (! Drupal.settings.ucLocation) {
    Drupal.settings.ucLocation = {};
  }
  
  if (! Drupal.settings.ucLocation.initialized) {
    $('#edit-panes-delivery-delivery-address-select,#edit-panes-billing-billing-address-select').change( 
      function (evt) {
        if (evt.target.id == 'edit-panes-delivery-delivery-address-select') {
          uc_location_apply_address("delivery", evt.target.value);
        } 
        else if (evt.target.id == 'edit-panes-billing-billing-address-select') {
          uc_location_apply_address("billing", evt.target.value);
        }
      });
  
    Drupal.settings.ucLocation.initialized = true;
  }
}

function uc_location_apply_address(type, address_str) {
  if (address_str == "0") {
    return;
  }
  eval("var address = " + address_str + ";");
  var pane = "edit-panes-" + type + "-" + type;
  
  document.getElementById(pane + "-first-name").value = address.first_name;
  document.getElementById(pane + "-last-name").value = address.last_name;
  document.getElementById(pane + "-company").value = address.company;
  document.getElementById(pane + "-phone").value = address.phone;
  document.getElementById(pane + "-street1").value = address.street1;
  document.getElementById(pane + "-street2").value = address.street2;
  document.getElementById(pane + "-city").value = address.city;
  document.getElementById(pane + "-postal-code").value = address.postal_code;
  
  var country = document.getElementById(pane + "-country");
  if(country.value != address.country) {
    country.value = address.country;
    uc_update_zone_select(pane + "-country", address.zone);
  }
  else {
    $("#" + pane + "-zone").val(address.zone).trigger("change");
  }
}