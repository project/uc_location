<?php

/**
 * @file
 * Integrates Ubercart and location.
 *
 * The uc_location module loads addresses from user locations and 
 * saves addresses to user locations from Ubercart.
 *
* The Ubercart order process is altered so that users select delivery
 * and billing addresses from their list of user loctions rather
 * than from previous orders. Any new addresses entered during the
 * order process are automatically added to the user's location list.
 **/

/**
 * Implementation of hook_ca_predicate().
 **/
function uc_location_ca_predicate() {
  $predicates = array();

  // Setup a default predicate for customer checkout notifications.
  $predicates['uc_checkout_save_location'] = array(
    '#title' => t('Save locations to user account'),
    '#description' => t('Save locations to user account.'),
    '#class' => 'uc_location',
    '#status' => 1,
    '#weight' => -5,
    '#trigger' => 'uc_checkout_complete',
    '#actions' => array(
      array(
        '#name' => 'uc_location_save_location',
        '#title' => t('Save the user location'),
        '#argument_map' => array(
          'order' => 'order',
        ),
        '#settings' => array(
        ),
      ),
    ),
  );

  return $predicates;
}

/**
 * Implementation of hook_ca_action().
 */
function uc_location_ca_action() {
  $order_arg = array(
    '#entity' => 'uc_order',
    '#title' => t('Order'),
  );

  $actions['uc_location_save_location'] = array(
    '#title' => t("Save the billing / shipping addresses to the user's account locations"),
    '#category' => t('Location'),
    '#callback' => 'uc_location_save_location',
    '#arguments' => array(
      'order' => $order_arg,
    ),
  );

  return $actions;
}

/**
 * Implements hook_form_alter().
**/
function uc_location_form_uc_cart_checkout_form_alter(&$form, &$form_state) {
  global $user;
  
  $is_billable = !empty($form['panes']['billing']);
  $is_deliverable = !empty($form['panes']['delivery']) && (uc_cart_is_shippable() || !variable_get('uc_cart_delivery_not_shippable', TRUE));

  // If new user, remove the address selection fields.
  if ($user->uid == 0) {
    unset($form['panes']['delivery']['delivery_address_select']);
    unset($form['panes']['billing']['billing_address_select']);
  } 
  elseif (($is_billable || $is_deliverable) && $address_list = _uc_location_create_address_list()) {
    drupal_add_js(drupal_get_path('module', 'uc_location') .'/uc_location.js');
    
    if (user_access('set own user location')) {
      $address_book_icon = l(uc_store_get_icon('file:address_book', FALSE, 'address-book-icon'),
        'user/'. $user->uid .'/edit', array('html' => TRUE)); 
    }
    else {
      $address_book_icon = '';
    }
    
    if ($is_billable) {
      _uc_location_recreate_address_select_field($form['panes']['billing'], 'billing', $address_list, $address_book_icon);
    }
      
    if ($is_deliverable) {
      _uc_location_recreate_address_select_field($form['panes']['delivery'], 'delivery', $address_list, $address_book_icon);
    }
  }
}

/**
 * Saves new locations if billing and/or delivery location are not already in locations list
 * (callback function from uc_location_ca_action).
 * 
 * @param $order  
 *   An object containing the order information.
 * @param $settings  
 *   A keyed array of conditional actions (ca) configuration information.
**/
function uc_location_save_location(&$order, $settings) {
  $locations = location_load_locations($order->uid, 'uid');
  $orders = array('billing' => '', 'delivery' => '');
  
  foreach ($orders as $type => $value) {
    $order_location = _uc_location_order_to_location($order, $type);
    $is_order_empty = _uc_location_location_is_empty($order_location);
    $order_str = _uc_location_order_to_str($order_location);
    $orders[$type] = $order_str;
    $orders_are_equal = ($orders['billing'] == $orders['delivery']);

    // Add a location when the order info exists, the delivery order isn't the same
    // as the billing (so a duplicate isn't added), and the order info doesn't 
    // already exist in locations. A duplicate is added when billing & delivery are
    // the same and gmap doesn't recognize the province. This results in the province
    // and province_name fields being set to empty, which means that the delivery info
    // won't match the billing info already added to locations, even though they are
    // the same.
    if ($is_order_empty == FALSE && $orders_are_equal == FALSE) {
      $index = _uc_location_find($locations, $order_str);
      
      if ($index === FALSE) {
        $set_primary = ( ($type == 'billing') && (_uc_location_primary_not_set($locations)) );
        _uc_location_add_to_locations($order->uid, $order_location, $locations, $set_primary);
      }
    }
  }
}

/********************************************************************
  Utility functions for uc_location_form_uc_cart_checkout_form_alter
*********************************************************************/

/**
 * Creates address list from user locations.
 *
 * @return
 *   A keyed array of user location addresses in json format.
**/
function _uc_location_create_address_list() {
  global $user;
  
  // Reload user account as the locations are not always loaded.
  $account = user_load($user->uid);
  
  $address_list = array('0' => t('Select one...'));
  
  foreach ($account->locations as $location) {
    $uc_address = _uc_location_to_uc_address($location);
  
    if ($location['name']) {
      $address_list[drupal_to_js($uc_address)] = $location['name'];
    } 
    else {
      $address_list[drupal_to_js($uc_address)] = _uc_location_format($location);
    }
  }
  
  return $address_list;
}

/**
 * Recreates delivery or billing address selection field.
 *
 * @param $pane  
 *   An array of form section elements to edit.
 * @param $type  
 *   A string containing the type of order ('billing' or 'delivery').
 * @param $address_list
 *   A keyed array of user location addresses in json format.
 * @param $address_book_icon
 *   A string containing an address book icon icon and link to editing user profile.
**/
function _uc_location_recreate_address_select_field(&$pane, $type, &$address_list, $address_book_icon) {
  $pane['#description'] = t('Edit the address below or select an address from the list.');
  $pane[$type .'_address_select'] = array(
    '#type' => 'select',
    '#title' => t('Saved addresses'),
    '#options' => $address_list,
    '#suffix' => $address_book_icon,
    '#weight' => -10,
  );
}

/**
 * Converts user location to Ubercart address format (used by apply_address js function).
 *
 * @param $location
 *   An address in "location" format (keyed array as per location_load_locations()).
 * @return
 *   The address in Ubercart address format.
**/
function _uc_location_to_uc_address(&$location) {
  $address = array();
  
  // Use first, last & organization fields if module installed.
  if (module_exists('location_first_last_org')) {
    $address['first_name'] = $location['first_name'];  
    $address['last_name'] = $location['last_name'];
    $address['company'] = $location['organization'];
  }
  else {
    $address['first_name'] = '';  
    $address['last_name'] = '';
    $address['company'] = '';
  }
  
  $address['name'] = $location['name'];
  $address['street1'] = $location['street'];
  $address['street2'] = $location['additional'];
  $address['city'] = $location['city'];
  
  $address['zone'] = $location['province_name'];
  $address['postal_code'] = $location['postal_code'];
  
  $country = ($location[country] == 'uk' ? 'GB' : drupal_strtoupper($location[country]));

  // apply_address js function compares iso_code_2 so need to look it up
  $query = "SELECT country_id FROM {uc_countries} WHERE country_iso_code_2 = '%s'";
  $country = db_fetch_object( db_query($query, $country) );
  
  // In case location stores a different code and query returns null as a result, lookup by name
  if (empty($country)) {
    $query = "SELECT country_id FROM {uc_countries} WHERE country_name = '%s'";
    $country = db_fetch_object( db_query($query, drupal_strtoupper($location['country_name'])) );
  }
  
  $address['country'] = $country->country_id;
  $address['phone'] = isset($location['phone']) ? $location['phone'] : '';
  
  return $address;
}

/**
 * Converts location to formatted address (US).
 * Used so we don't need to rely on uc_format_address() which is more 
 * dynamic and may break the "apply_address()" javascript function.
 *
 * @param $location
 *   An address in "location" format (keyed array as per location_load_locations() ).
 * @return
 *   The address formatted for display.
**/
function _uc_location_format(&$location) {
  $country = drupal_strtoupper($location['country']);
  $format_str = '';
  
  // Use first, last & organization fields if module installed.
  if (module_exists('location_first_last_org')) {
    $first = $location['first_name'];
    $last = $location['last_name'];
    $org = $location['organization'];
    
    if (!empty($first) && !empty($last)) {
      $format_str .= "$first $last, ";
    }
    else if (!empty($first)) {
      $format_str .= "$first, ";
    }
    else if (!empty($last)) {
      $format_str .= "$last, ";
    }
    
    if (!empty($org)) {
      $format_str .= "$org, ";
    }
  }
  
  $format_str .= "{$location['street']}, {$location['additional']}, {$location['city']}, " . 
    "{$location['province']} {$location['postal_code']}, {$country}";
  
  $format_str = str_replace('  ', ' ', $format_str);
  $format_str = str_replace(', ,', ',', $format_str);
  
  // Check to see if end of string has comma space and remove if so.
  if (drupal_substr($format_str, -2, drupal_strlen($format_str)) == ', ') {
    $format_str = drupal_substr($format_str, 0, -2);
  }
  
  return $format_str;
}

/*************************************************
  Utility functions for uc_location_save_location
**************************************************/

/**
 * Adds new location to user locations and sets location to primary if set_primary.
 * 
 * @param $uid
 *   A string containing the user id.
 * @param $location
 *   An array containing the user location to add.
 * @param $locations
 *   The user locations array as returned by location_load_locations().
 * @param $set_primary
 *    TRUE if location is_primary is to be set to 1.
**/
function _uc_location_add_to_locations($uid, &$location, &$locations, $set_primary) {
  if ($set_primary) {
    $location['is_primary'] = 1;
  }
  $criteria = array('uid' => $uid);
  $locations[] = $location;

  location_save_locations($locations, $criteria);
}

/**
 * Converts order (object) to location format (keyed array).
 *
 * @param $order
 *   An object containing the order information.
 * @param $type
 *   A string containing the type of order ('billing' or 'delivery').
 * @return
 *   A location (keyed array).
 * 
**/
function _uc_location_order_to_location(&$order, $type) {
  // Queries database for the zone code and name based on order zone number.
  $query_str = "SELECT zone_code, zone_name FROM {uc_zones} WHERE zone_id = %d";
  $zone = db_fetch_object( db_query($query_str, $order->{$type .'_zone'}) );
  
  // Queries database for the country iso code and name based on order country number.
  $query_str = "SELECT country_iso_code_2 as country_code, country_name FROM {uc_countries} WHERE country_id = %d";
  $country = db_fetch_object( db_query($query_str, $order->{$type .'_country'}) );

  // If query returns false instead of of query object, create object with code and empty name value.
  if ($zone == FALSE) {
    $zone = (object) array(
        'zone_code' => $order->{$type .'_zone'} ? $order->{$type .'_zone'} : '',
        'zone_name' => '',
      );
  }
  if ($country == FALSE) {
    $country = (object) array(
        'country_code' => $order->{$type .'_country'},
        'country_name' => '',
      );
  }
  
  // Now copy info into location array.
  $location = array();
  
  // Use first & last name, organization fields if module installed.
  if (module_exists('location_first_last_org')) {
    $location['first_name'] = trim($order->{$type .'_first_name'});  
    $location['last_name'] = trim($order->{$type .'_last_name'}); 
    $location['organization'] = trim($order->{$type .'_company'});  
  }
  
  $location['street'] = trim($order->{$type .'_street1'});
  $location['additional'] = trim($order->{$type .'_street2'});
  $location['city'] = trim($order->{$type .'_city'});
  $location['province'] = $zone->zone_code;
  $location['province_name'] = $zone->zone_name;
  $location['postal_code'] = trim($order->{$type .'_postal_code'});
  
  // Convert United Kingdom code to Location-compatible code (GB)
  if($country->country_code == 'GB') {
    $location['country'] = 'uk';
  }
  else {
    // Location stores country code in lower case.
    $location['country'] = drupal_strtolower($country->country_code);
  }
  
  $location['country_name'] = $country->country_name;
  $location['phone'] = trim($order->{$type .'_phone'});
  
  return $location;
}

/**
 * Checks if location field values are empty.
 *
 * @param $location
 *   A location (keyed array).
 * @return
 *   TRUE if location is empty.
**/
function _uc_location_location_is_empty($location) {
  // Have to remove country code & name values because 
  // they have a default value whether form pane shows or not
  // working with copy of $order_location (pass by value)
  $location['country'] = '';
  $location['country_name'] = '';
  
  // removes blank values from array when no callback specified
  $arr = array_filter($location);
  return empty($arr);
}

/**
 * Finds index of location information in user locations.
 * Note: this currently misses some situations with non-US addresses
 * where the province has been removed (e.g., by gmap location).
 *
 * @param $locations
 *   The user locations array as returned by location_load_locations().
 * @param $location_str
 *   A string containing the location to find.
 * @return 
 *   The integer index or FALSE.
**/
function _uc_location_find(&$locations, $location_str) {
  $len = count($locations);
  
  for ($i=0; $i <= $len; $i++) {
    $location_index_str = _uc_location_location_to_str($locations[$i]);
    if ($location_index_str == $location_str) {
      return $i;
    }
  }
  
  return FALSE;  
}

/**
 * Converts order to lowercase json string format.
 *
 * @param $order
 *   An order (keyed array).
 * @return
 *   A lowercase json string.
**/
function _uc_location_order_to_str($order) {
  // Country name can vary between Ubercart and Location, so remove.
  unset($order['country_name']);
  return drupal_strtolower(drupal_to_js($order));
}

/**
 * Converts location to lowercase json string format.
 *
 * @param $location
 *   A location (keyed array).
 * @return
 *   A lowercase json string.
**/
function _uc_location_location_to_str(&$location) {
  $fields = array();
  
  // Use first, last & organization fields if module installed.
  if (module_exists('location_first_last_org')) {
    $fields = array_merge($fields, array('first_name', 'last_name', 'organization'));
  }
  
  // No longer using country_name because it can vary between Ubercart and Location
  $fields = array_merge($fields, array('street', 'additional', 'city', 'province', 'province_name', 'postal_code', 'country', 'phone'));
  $address = array();
  
  // Just copy the specified fields.
  foreach ($fields as $field) {
    $address[$field] = $location[$field];
  }
  
  return drupal_strtolower(drupal_to_js($address));
}

/**
 * Checks user locations to see if any location has is_primary set to 1;
 *
 * @param $locations
 *   The user locations array as returned by location_load_locations().
 * @return 
 *   TRUE if no location's is_primary field is set to 1;
**/
function _uc_location_primary_not_set(&$locations) {
  foreach ($locations as $location) {
    if ($location['is_primary'] == 1) {
      return FALSE;
    }
  }
  
  return TRUE;
}