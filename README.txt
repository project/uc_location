
-- SUMMARY --

This module integrates Ubercart with Location. When a user places an order, their
addresses go into their user locations. When placing new orders, all addresses from
the location module are available for the user to choose from.

Managing addresses is handled by the Location module, and this allows connection
with GMap and other Location aware modules.

The current release has been tested against Ubercart 2.x-rc3.

For a full description of the module, visit the project page:
  http://drupal.org/project/uc_location

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/uc_location


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* This is configured using Ubercart conditional actions.


-- CONTACT --

Current maintainers:
* Bibek Sahu (bibeksahu) - http://drupal.org/user/536246
* Mark Earnest (mearnest) - http://drupal.org/user/606880

This project has been sponsored by:
* Trees for Life
  Creating hope through a movement in which people join hands to break the 
	cycle of poverty and hunger and care for our Earth. Visit 
	http://www.treesforlife.org for more information.

* Based on uc_addresses by Tony Freixas (http://drupal.org/project/uc_addresses).
